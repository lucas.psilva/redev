const { app, server} = require('../server')
const request = require('supertest');
const mongoose = require('mongoose');
let res = null

beforeAll(() =>{
  res = request(app)
})

describe('GET /topic', function() {
    it('responds with json list', function(done) {
      res.get('/topic')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
    });

    it('should post a new topic', function(done) {
        res.post('/topic')
        .send({title: 'test'})
          .set('Accept', 'application/json')
          .expect('Content-Type', /json/)
          .expect(200)
          .end(function(err, res) {
            if (err) return done(err);
            done();
          });
      });
  });

afterAll(async ()=>{
    server.close()
    await mongoose.connection.close()
})